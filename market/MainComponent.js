'use strict';

import React from 'react';
import {AppRegistry, StyleSheet, Text, View, Dimensions} from 'react-native';
import {Scene, Router, Actions} from 'react-native-router-flux';

import Stickers from './Stickers/Stickers';
import Services from './Services/Services';
import StickerDetails from './Stickers/StickerDetails';
import Loading from './elements/Loading';

import Config from './Helper/Config';

global.appToken = '';
global.themeColor = '';
var loading;

const width = Dimensions
  .get("window")
  .width;
const height = Dimensions
  .get("window")
  .height;
var slider;
export default class MainComponent extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      // initalScene: this.props.type,
      initalScene: 'stickers'
    }
  }

  async loadData() {
    await Config.set('userInfo', {
      "token": this.props.token,
      "userAgent": this.props.userAgent,
      "themeColor": this.props.themeColor
    });
  }

  render() {
    const scenes = Actions.create(
      <Scene key="root">
        <Scene
          key="Stickers"
          component={Stickers}
          hideNavBar
          initial={this.state.initalScene == 'stickers'}/>
        <Scene key="StickerDetails" component={StickerDetails} hideNavBar/>
        <Scene
          key="Services"
          component={Services}
          hideNavBar
          initial={this.state.initalScene != 'stickers'}/>
      </Scene>
    );

    this.loadData();
    return <Router scenes={scenes}/>;
  }
}