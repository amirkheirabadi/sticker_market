'use strict';

import Config from './Config';
import MarketHelper from './MarketHelper';

class Api {
    async request(method, url) {
        let userInfo = await Config.get('userInfo');
        let response = await fetch('https://api2.gapafzar.com/v1/' + url, {
            method: method,
            headers: {
                'X-Token': userInfo['token'],
                'User-Agent': userInfo['userAgent']
            }
        });
        console.log(response);
        let res = await response.json();
        return res;
    }
};

export default new Api();