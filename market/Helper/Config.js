import MarketHelper from './MarketHelper';
import {AsyncStorage} from 'react-native';

class Config {
    async set(key, value) {
        try {
            await AsyncStorage.setItem(key, JSON.stringify(value));
            return true;
        } catch (e) {
            return false;
        }
    }

    async get(key) {
        try {
            let data = await AsyncStorage.getItem(key);
            return JSON.parse(data);
        } catch (e) {
            return false;
        }
    }
}

export default new Config();