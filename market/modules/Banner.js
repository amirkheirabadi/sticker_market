'use strict';
import React from 'react';
import {Text, View, TouchableWithoutFeedback, Dimensions} from 'react-native';

import Image from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';
import SwipeALot from 'react-native-swipe-a-lot';
import Navbar from '../elements/Navbar';
import Styles from '../styles';
import Config from '../Helper/Config';

const width = Dimensions
  .get("window")
  .width;
const height = Dimensions
  .get("window")
  .height;
var slider;
export default class Banner extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      data: this.props.data,
      themeColor: '#fff'
    };
  }

  async componentDidMount() {
    let themeColor = await Config.get('themeColor');
    slider.swipeToPage(this.state.data.module_data.length - 1)
    this.setState({
      'themeColor': '#' + themeColor
    });
  }

  render() {
    return (
      <View
        style={[
        Styles.moduleWrapper,
        Styles.sliderWrapper, {
          height: 206
        }
      ]}>
        <SwipeALot
          ref={(ref) => slider = ref}
          Autoplay={true}
          circleDefaultStyle={Styles.sliderLayout}
          circleActiveStyle={{
          backgroundColor: this.state.themeColor
        }}>
          {this
            .state
            .data
            .module_data
            .map((row, index) => {
              return <TouchableWithoutFeedback
                onPress={() => {
                this
                  .props
                  .stickerDetail(row._id)
              }}>
                <Image
                  threshold={1000}
                  resizeMode="stretch"
                  source={{
                  uri: row.banner[700]
                }}
                  style={{
                  width: width,
                  height: 206
                }}/>
              </TouchableWithoutFeedback>
            })}
        </SwipeALot>
      </View>
    )
  }
}