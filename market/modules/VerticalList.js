'use strict';

import React from 'react';
import {Text, View, TouchableWithoutFeedback, Dimensions} from 'react-native';
import Button from '../elements/Button';

import GridView from 'react-native-super-grid';
import Styles from '../styles';
import Image from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';

export default class VerticalList extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      data: this.props.data.module_data
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.changes.length == 2) {
      var data = this.state.data;
      data.map((row, index) => {
        if (row.name == nextProps.changes[0]) {
          switch (nextProps.changes[1]) {
            case "add":
              return row['has_installed'] = "yes";
            case "remove":
              return row['has_installed'] = "no";
          }
          return row;
        }
      });
      this.setState({data: data});
    }
  }

  pressButton = (name, has_installed) => {
    if (has_installed == "yes" && this.props.removeSticker(name)) {
      return "install";
    } else if (this.props.addSticker(name)) {
      return "delete";
    }
    return false;
  }

  render() {
    const width = Dimensions
      .get("window")
      .width;
    const height = Dimensions
      .get("window")
      .height;
    return (
      <View style={[Styles.VerticalModuleWrapper]}>
        <View style={[Styles.titleWrapperModule, {}]}>
          <Text style={[Styles.iranSans, Styles.titleModule]}>استیکرهای تصادفی</Text>
        </View>
        <GridView
          itemWidth={(width / 4) - 10}
          items={this.state.data}
          style={[{
            padding: 0,
            marginBottom: 60
          }
        ]}
          renderItem={item => (
          <TouchableWithoutFeedback
            onPress={() => {
            this
              .props
              .stickerDetail(item._id)
          }}>
            <View style={[Styles.GridItemWrapper, {}]}>
              <Image
                threshold={1000}
                resizeMode="contain"
                source={{
                uri: item.cover[256]
              }}
                style={[Styles.moduleThumb]}/>
              <Text style={[Styles.iranSans, Styles.lisTItemTitle]}>{item.title}</Text>
              <Button
                type={item.embed == "yes"
                ? "embed"
                : item.has_installed}
                onPress={() => {
                return this.pressButton(item.name, item.has_installed)
              }}
                parameter={item.name}/>
            </View>
          </TouchableWithoutFeedback>
        )}/>
      </View>
    )
  }
}