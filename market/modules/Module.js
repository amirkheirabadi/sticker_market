'use strict';

import React from 'react';
import {View} from 'react-native';
import Config from '../Helper/Config';
import {Actions} from 'react-native-router-flux';
import Banner from './Banner';
import HorizontalSmallList from './HorizontalSmallList';
import VerticalList from './VerticalList';
import StickerDetails from './StickerDetail';
import Api from '../Helper/Api';

var _this;
export default class Module extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            changes: []
        };
        _this = this;
    }

    renderModule = (module) => {
        if (module.module.model) {
            switch (module.module.layout) {
                case 'horizontal_small_list':
                    return <HorizontalSmallList
                        changes={this.state.changes}
                        data={module}
                        removeSticker={this.removeSticker}
                        addSticker={this.addSticker}
                        stickerDetail={this.stickerDetail}/>
                case 'banner':
                    return <Banner
                        changes={this.state.changes}
                        data={module}
                        removeSticker={this.removeSticker}
                        addSticker={this.addSticker}
                        stickerDetail={this.stickerDetail}/>
                case 'vertical_list':
                    return <VerticalList
                        changes={this.state.changes}
                        data={module}
                        removeSticker={this.removeSticker}
                        addSticker={this.addSticker}
                        stickerDetail={this.stickerDetail}/>
                case 'detail':
                    return <StickerDetails
                        changes={this.state.changes}
                        data={module}
                        removeSticker={this.removeSticker}
                        addSticker={this.addSticker}
                        stickerDetail={this.stickerDetail}/>
            }
        }
    }

    stickerDetail = (id) => {
        Actions.StickerDetails({stickerId: id, type: "reset"});
    }

    async addSticker(name) {
        let response = await Api.request("GET", "user/addSticker/" + name + ".json");
        if (response.code == 200) {
            _this.setState({
                changes: [name, 'add']
            });
            _this
                .props
                .updateStatus(name, "add");
            return true;
        }
        return false
    }

    async removeSticker(name) {
        let response = await Api.request("GET", "user/removeSticker/" + name + ".json");
        if (response.code == 200) {
            _this.setState({
                changes: [name, 'remove']
            });
            _this
                .props
                .updateStatus(name, "remove");
            return true;
        }
        return false;
    }

    render() {
        return (
            <View>
                {this
                    .props
                    .data
                    .map((module) => {
                        return <View>{this.renderModule(module)}</View>;
                    })}
            </View>
        )
    }
}