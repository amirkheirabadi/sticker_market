'use strict';

import React from 'react';
import {Text, View, TouchableWithoutFeedback, Dimensions, ScrollView} from 'react-native';

import Button from '../elements/Button';
import Image from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';
import Navbar from '../elements/Navbar';
import Styles from '../styles';

var _scrollView;
export default class HorizontalSmallList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            data: this.props.data.module_data,
            title: this.props.data.module.title
        };
    }

    componentDidMount() {
        setTimeout(function () {
            _scrollView.scrollToEnd({animated: false});
        }, 10);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.changes.length == 2) {
            var data = this.state.data;
            data.map((row, index) => {
                if (row.name == nextProps.changes[0]) {
                    switch (nextProps.changes[1]) {
                        case "add":
                            return row['has_installed'] = "yes";
                        case "remove":
                            return row['has_installed'] = "no";
                    }
                    return row;
                }
            });
            this.setState({data: data});
        }
    }

    pressButton = (name, has_installed) => {
        if (has_installed == "yes" && this.props.removeSticker(name)) {
            return "install";
        } else if (this.props.addSticker(name)) {
            return "delete";
        }
        return false;
    }

    render() {
        return (
            <View style={[Styles.moduleWrapper]}>
                <View style={[Styles.titleWrapperModule]}>
                    <Text style={[Styles.iranSans, Styles.titleModule]}>{this.state.title}</Text>
                </View>
                <ScrollView
                    scrollEventThrottle={200}
                    showsHorizontalScrollIndicator={false}
                    horizontal={true}
                    style={[Styles.listWrapperModule]}
                    ref={(scrollView) => {
                    _scrollView = scrollView;
                }}>
                    {this
                        .state
                        .data
                        .map((row, index) => {
                            return <TouchableWithoutFeedback
                                onPress={() => {
                                this
                                    .props
                                    .stickerDetail(row._id)
                            }}>
                                <View style={[Styles.listItemModule]}>
                                    <Image
                                        threshold={1000}
                                        resizeMode="contain"
                                        source={{
                                        uri: row.cover[256]
                                    }}
                                        style={[Styles.moduleThumb]}/>
                                    <Text style={[Styles.iranSans, Styles.lisTItemTitle]}>{row.title}</Text>
                                    <Button
                                        type={row.embed == "yes"
                                        ? "embed"
                                        : row.has_installed}
                                        onPress={() => {
                                        return this.pressButton(row.name, row.has_installed)
                                    }}
                                        parameter={row.name}/>
                                </View>
                            </TouchableWithoutFeedback>
                        })
                }
                </ScrollView>
            </View>
        )
    }
}