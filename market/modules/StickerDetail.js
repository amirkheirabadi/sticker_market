import React from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Navigator,
    TouchableWithoutFeedback,
    Dimensions,
    ScrollView,
    Modal,
    BackAndroid
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Image from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';

import Navbar from '../elements/Navbar';
import Button from '../elements/Button';
import Styles from '../styles';
import GridView from 'react-native-super-grid';
import Api from '../Helper/Api';
import Config from '../Helper/Config';
import Loading from '../elements/Loading';
import Module from '../modules/Module';

var loading;
var moduleHandle;
var _this;

export default class StickerDetail extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            stickerTitle: this.props.data.module_data.title,
            stickerDesc: this.props.data.module_data.description,
            stickerCover: this.props.data.module_data.cover[512],
            stickerItems: this.props.data.module_data.sticker_items,
            stickerHasInstalled: this.props.data.module_data.has_installed,
            stickerName: this.props.data.module_data.name,
            stickerId: this.props.data.module_data._id,

            themeColor: '#fff',
            showPreview: false,
            imagePreview: '',

            moduleData: []
        };
        _this = this;
    }

    pressButton = (name, has_installed) => {
        if (has_installed == "yes" && this.props.removeSticker(name)) {
            this.setState({stickerHasInstalled: "no"});
            return "install";
        } else if (this.props.addSticker(name)) {
            this.setState({stickerHasInstalled: "yes"});
            return "delete";
        }
        return false;
    }

    async componentDidMount() {
        let themeColor = await Config.get('themeColor');
        this.setState({
            'themeColor': '#' + themeColor
        });
    }

    render() {
        const width = Dimensions
            .get("window")
            .width;
        const height = Dimensions
            .get("window")
            .height;
        return (
            <View style={[Styles.moduleWrapper]}>
                <Modal
                    animationType={"fade"}
                    transparent={true}
                    onRequestClose={() => this.setState({showPreview: false})}
                    visible={this.state.showPreview}>
                    <View style={[Styles.stickerPreviewModal]}>
                        <View style={{}}>
                            <Image
                                resizeMode="contain"
                                threshold={1000}
                                source={{
                                uri: this.state.imagePreview
                            }}
                                style={[Styles.stickerPreview]}/>
                        </View>
                    </View>
                </Modal>

                <View style={[Styles.detailsInfoWrapper]}>

                    <View style={[Styles.infoWrapper]}>
                        <Text style={[Styles.iranSans, Styles.detailsTitle]}>{this.state.stickerTitle}</Text>
                        <Text style={[Styles.iranSans, Styles.detailsPrice]}>قیمت : رایگان</Text>

                        <View style={[Styles.stickerInfoButtonWrapper]}>
                            <Button
                                type={this.props.data.module_data.embed == "yes"
                                ? "embed"
                                : this.state.stickerHasInstalled}
                                buttonStyle={Styles.stickerInfoButton}
                                textStyle={Styles.stickerInfoButtonText}
                                onPress={() => {
                                return this.pressButton(this.state.stickerName, this.state.stickerHasInstalled)
                            }}
                                parameter={this.state.stickerName}/>
                        </View>

                        <Text style={[Styles.iranSans, Styles.detailsDescription]}>{this.state.stickerDesc}</Text>
                    </View>
                    <View style={[Styles.detailsCoverWrapper]}>
                        <Image
                            threshold={1000}
                            resizeMode="contain"
                            source={{
                            uri: this.state.stickerCover
                        }}
                            style={Styles.detailsCover}/>
                    </View>
                </View>

                <View style={[Styles.stickerItemsWrapper]}>

                    <GridView
                        itemWidth={(width / 3) - 30}
                        items={this.state.stickerItems}
                        style={[Styles.stickerItemsLayout]}
                        renderItem={item => (
                        <TouchableWithoutFeedback
                            onLongPress={() => {
                            var uri = "";
                            if (item.animate == "yes") {
                                uri = item.file.animate[256]['webp'];
                            } else {
                                uri = item.file.static[256]['png'];
                            }
                            this.setState({imagePreview: uri, showPreview: true});
                        }}
                            onPressOut={() => {
                            this.setState({showPreview: false})
                        }}>
                            <View
                                style={[{
                                    alignItems: 'center'
                                }
                            ]}>
                                <Image
                                    threshold={1000}
                                    resizeMode="contain"
                                    source={{
                                    uri: item.file.static[256]['png']
                                }}
                                    style={[Styles.moduleStickerDetail]}/>
                            </View>
                        </TouchableWithoutFeedback>
                    )}/>
                </View>
            </View>
        )
    }
}