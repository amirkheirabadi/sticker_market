'use strict';

import React from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Navigator,
    TouchableWithoutFeedback
} from 'react-native';
import Styles from '../styles';
import Config from '../Helper/Config';
import SubmitButton from './SubmitButton';

var themeColor;
export default class Button extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonState: this.props.type == "yes"
                ? "delete"
                : "install"
        }
    }

    async componentDidMount() {
        themeColor = "#" + await Config.get('themeColor');
    }

    onSubmit = () => {
        var result = this
            .props
            .onPress(this.props.parameter);
        this.setState({buttonState: result});
    }

    render() {
        return (
            <View
                style={{
                alignItems: 'center',
                width: 100
            }}>
                {this.props.type == "embed"
                    ? (
                        <Text
                            style={[
                            Styles.iranSans, {
                                color: '#fff',
                                backgroundColor: '#34495e',
                                paddingHorizontal: 15,
                                paddingVertical: 5,
                                marginVertical: 5,
                                fontSize: 13,
                                borderRadius: 50
                            }
                        ]}>
                            پیش فرض
                        </Text>
                    )
                    : (<SubmitButton
                        onSubmit={this.onSubmit}
                        buttonState={this.state.buttonState}
                        primaryColor={themeColor}
                        height={40}
                        width={100}
                        buttonInstall="نصب"
                        buttonStyle={[
                        {
                            alignSelf: 'center',
                            padding: 0,
                            borderRadius: 4,
                            backgroundColor: 'transparent'
                        },
                        this.props.buttonStyle
                    ]}
                        textStyle={[
                        Styles.iranSans, {
                            color: '#fff',
                            paddingHorizontal: 30,
                            paddingVertical: 5,
                            fontSize: 13,
                            borderRadius: 50,
                            backgroundColor: '#02b80c'
                        },
                        this.props.textStyle
                    ]}/>)}
            </View>
        )
    }
}