import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  buttonContainer: {
    justifyContent: 'center'
  },
  button         : {
    alignItems     : 'center',
    justifyContent : 'center',
    backgroundColor: 'white',
  },
  text           : {
    fontSize       : 22,
    // fontFamily     : 'proximanova-regular',
    backgroundColor: 'transparent'
  }
});
