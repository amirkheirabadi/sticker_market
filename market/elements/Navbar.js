'use strict';

import React from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Navigator,
    TouchableWithoutFeedback
} from 'react-native';
import Styles from '../styles';
import {Actions} from 'react-native-router-flux';
import Config from '../Helper/Config';
import MarketHelper from '../Helper/MarketHelper';

export default class Navbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            themeColor: '#7f57de'
        }
    }

    async componentDidMount() {
        let userInfo = await Config.get('userInfo');

        this.setState({themeColor: userInfo['themeColor'], title: this.props.title});
    }

    action = () => {
        if (this.props.action == "back") {
            Actions[this.props.target]({type: "reset"});
        } else {
            MarketHelper.exitApp();
        }
    }

    render() {
        return (
            <View style={[Styles.navbarWrapper]}>
                <TouchableWithoutFeedback
                    onPress={() => {
                    this.action();
                }}>
                    {this.props.action == "back"
                        ? (<Image
                            source={require("../resources/img/go-back-left-arrow.png")}
                            style={[Styles.navbarButton]}/>)
                        : (<Image
                            source={require("../resources/img/close-button.png")}
                            style={[Styles.navbarButton]}/>)}
                </TouchableWithoutFeedback>
                <Text style={[Styles.iranSans, Styles.navbarText]}>{this.state.title}</Text>
            </View>
        )
    }
}