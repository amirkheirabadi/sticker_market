import React, {Component, PropTypes} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Animated,
  Dimensions,
  Easing
} from 'react-native';

import {AnimatedCircularProgress} from 'react-native-circular-progress';

import styles from './submit.style';

const {width, height} = Dimensions.get('window');

const AnimatingCicleBackgroundColor = "#bbbbbb";
console.disableYellowBox = true;

class SubmitButton extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      isLoading: false,
      animatedValue: new Animated.Value(0),
      fill: 0,
      canShowAnimatedCircle: false,
      isReady: false
    }
  };

  static defaultProps = {
    width: 180,
    height: 54,
    installColor: '#2ecc71',
    deleteColor: '#e74c3c',
    circleColor: '#9b59b6',
    buttonInstall: 'نصب',
    buttonDelete: 'حذف',
    onSubmit: () => {},
    buttonState: 'install',
    animationDuration: 100
  };

  componentWillReceiveProps(nextProps) {
    setTimeout(() => {
      const buttonState = nextProps.buttonState;
      if (buttonState === 'install' || buttonState === 'delete') {
        this.setState({
          isLoading: false,
          isReady: true,
          canShowAnimatedCircle: false,
          buttonState: buttonState
        }, this.animateBackToOriginal)
      }
    }, 1000)
  }

  renderButton() {
    const {
      width,
      height,
      installColor,
      deleteColor,
      buttonStyle,
      buttonState,
      circleColor,
      buttonInstall,
      buttonDelete
    } = this.props;
    const {animatedValue, isLoading, isReady, canShowAnimatedCircle, fill} = this.state;
    const buttonWidth = animatedValue.interpolate({
      inputRange: [
        0, 1, 2
      ],
      outputRange: [width, height, width]
    });
    const buttonHeight = height;
    const readyStateBorderColor = buttonState === 'install'
      ? installColor
      : deleteColor;
    let borderColor = installColor;
    borderColor = isLoading
      ? AnimatingCicleBackgroundColor
      : readyStateBorderColor;
    const circleColorState = buttonState === 'install' ? deleteColor: installColor;
    const readyStateBgColor = buttonState === 'install'
      ? installColor
      : deleteColor;
    const backgroundColor = isLoading
      ? 'transparent'
      : !isReady
        ? deleteColor
        : readyStateBgColor;
    const buttonOpacity = canShowAnimatedCircle
      ? 0
      : 1;

    return (
      <Animated.View
        style={[
        styles.buttonContainer, {
          width: buttonWidth
        }
      ]}>
        <TouchableOpacity
          style={[
          styles.button,
          buttonStyle, {
            height: buttonHeight,
            borderColor: borderColor,
            opacity: buttonOpacity
          }
        ]}
          onPress={this.onSubmitButtonClick}>
          {this.renderBody()}
        </TouchableOpacity>
        {this.renderAnimatedCircle(animatedValue, fill, height, circleColorState)}
      </Animated.View>
    );
  }

  render() {
    return (
      <View>
        {this.renderButton()}
      </View>
    );
  }

  renderBody = () => {
    const {
      buttonTextWhenReady,
      iconName,
      textStyle,
      buttonInstall,
      buttonDelete,
      deleteColor,
      buttonState,
      installColor
    } = this.props;
    const {animatedValue, isReady} = this.state;
    const textOpacity = animatedValue.interpolate({
      inputRange: [
        0, 0.5, 1, 1.5, 2
      ],
      outputRange: [1, 0, 0, 0, 1]
    });

    if (buttonState == "install") {
      return (
        <Animated.Text
          style={[
          styles.text,
          textStyle, {
            opacity: textOpacity,
            color: "#fff",
            backgroundColor: installColor
          }
        ]}>{buttonInstall}</Animated.Text>
      );
    } else {
      return (
        <Animated.Text
          style={[
          styles.text,
          textStyle, {
            opacity: textOpacity,
            color: "#fff",
            backgroundColor: deleteColor
          }
        ]}>{buttonDelete}</Animated.Text>
      );
    }
  };

  renderAnimatedCircle = (animatedValue, fill, height, circleColor) => {
    const opacity = animatedValue.interpolate({
      inputRange: [
        0, 0.99, 1, 1.1, 2
      ],
      outputRange: [
        0, 0, 1, 0, 0
      ],
      extrapolate: 'clamp'
    });

    return (<AnimatedCircularProgress
      ref='circularProgress'
      size={height}
      width={4}
      fill={fill}
      tintColor={circleColor}
      backgroundColor={AnimatingCicleBackgroundColor}
      rotation={0}
      tension={20}
      style={{
      backgroundColor: 'transparent',
      position: 'absolute',
      top: 0,
      left: 0,
      opacity: opacity
    }}/>);
  };

  onSubmitButtonClick = () => {
    const {isLoading, isReady, animatedValue} = this.state;
    const {onSubmit, animationDuration, buttonState} = this.props;

    onSubmit(buttonState);
    this.setState({
      isLoading: true,
      fill: 0
    }, () => setTimeout(() => {
      this.setState({fill: 100, canShowAnimatedCircle: true})
    }, animationDuration));
    Animated
      .timing(animatedValue, {
      toValue: 1,
      duration: animationDuration,
      easing: Easing.linear
    })
      .start();
  };

  animateBackToOriginal = () => {
    const {animatedValue} = this.state;
    const {buttonState, onSuccess, animationDuration} = this.props;

    Animated
      .timing(animatedValue, {
      toValue: 2,
      duration: animationDuration,
      easing: Easing.linear
    })
      .start();
  }
}

SubmitButton.propTypes = {

  installColor  : PropTypes.string,
  deleteColor: PropTypes.string,

  buttonState: PropTypes.oneOfType( [ 'install', 'delete'] ),

  width              : PropTypes.number, // button width
  height             : PropTypes.number, // button height
  buttonInstall         : PropTypes.string, // button text. eg: Submit
  buttonDelete         : PropTypes.string, // button text. eg: Submit
  buttonTextWhenReady: PropTypes.string, // to show when success.Either pass this or pass icon name (any name from fontawesome lib)
  iconName           : PropTypes.string, // any name from font awesome lib
  textStyle          : PropTypes.oneOfType( [ PropTypes.number, PropTypes.object ] ), // button text style
  buttonStyle        : PropTypes.oneOfType( [ PropTypes.number, PropTypes.object ] ), // button style
  animationDuration  : PropTypes.number,
  errorColor         : PropTypes.color,

  onSubmit   : PropTypes.func, // function to be executed on button press.
  onSuccess  : PropTypes.func, // on success callback
  onError    : PropTypes.func // on error callback
};

module.exports = SubmitButton;
