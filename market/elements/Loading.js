'use strict';

import React from 'react';
import {
    Text,
    View,
    Image,
    Navigator,
    TouchableWithoutFeedback,
    Modal
} from 'react-native';
import Styles from '../styles';
import Config from '../Helper/Config';
import * as Progress from 'react-native-progress';

export default class Loading extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            themeColor: '#fff'
        }
    }

    async componentDidMount() {
        let userInfo = await Config.get('userInfo');
        this.setState({'themeColor': userInfo['themeColor']});

        if (this.props.default) {
            this.setState({show: true});
        }
    }

    hide = () => {
        this.setState({show: false});
    }

    show = () => {
        this.setState({show: false});
    }

    render() {
        return (
            <Modal animationType={"fade"} transparent={true} visible={this.state.show} onRequestClose={() => this.setState({show: false})}>
                <View style={[Styles.loadingWrapper]}>
                    <View style={Styles.loadingLayout}>

                        <Text style={[Styles.iranSans, Styles.loadingText]}>لطفا منتظر بمانید ...</Text>
                        <View style={[Styles.loadingSpinner]}>
                            <Progress.CircleSnail width={50} color={this.state.themeColor} size={50}/>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }
}