'use strict';

import React from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Navigator,
    TouchableWithoutFeedback,
    Dimensions,
    ScrollView,
    Modal,
    BackAndroid
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Image from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';

import Navbar from '../elements/Navbar';

import Styles from '../styles';
import Api from '../Helper/Api';
import Config from '../Helper/Config';
import Loading from '../elements/Loading';
import Module from '../modules/Module';

var loading;
var _this;
export default class StickerDetails extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            moduleData: [],
            stickerId: this.props.stickerId,
            loading: true
        };
        _this = this;
    }

    async componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress', () => {
            Actions.Stickers({type: "reset"});
            return true
        });
        let themeColor = await Config.get('themeColor');
        let response = await Api.request("GET", "modules/sticker_detail/" + this.state.stickerId + ".jsonp");
        this.setState({themeColor: themeColor, moduleData: response.data});
        loading.hide();
        this.setState({loading: false});
    }

    render() {
        return (
            <View
                style={{
                backgroundColor: 'rgb(238, 239, 241)'
            }}>
                <Loading default={true} ref={(ref) => loading = ref}/>
                {!this.state.loading ? <Navbar title="جزئیات استیکرها" action="back" target="Stickers"/>: null}
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={[Styles.detailLayout]}>
                        <Module data={this.state.moduleData}/>
                    </View>
                </ScrollView>
            </View>
        )
    }
}