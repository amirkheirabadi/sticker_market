'use strict';

import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  TouchableHighlight,
  ScrollView,
  Modal,
  Dimensions,
  BackAndroid
} from 'react-native';

import MarketHelper from '../Helper/MarketHelper';
import {Actions} from 'react-native-router-flux';

import Navbar from '../elements/Navbar';
import Loading from '../elements/Loading';
import Module from '../modules/Module';

import Styles from '../styles';
import Api from '../Helper/Api';
import Config from '../Helper/Config';

var loading;
export default class Stickers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      themeColor: '#fff',
      data: [],
      loading: true
    };
  }
  async componentDidMount() {
    BackAndroid.addEventListener('hardwareBackPress', () => {
      MarketHelper.exitApp();
      return true
    });
    let userInfo = await Config.get('userInfo');
    let response = await Api.request("GET", "modules/stickers.jsonp");

    this.setState({data: response.data, 'themeColor': userInfo['themeColor']});
    loading.hide();
    this.setState({loading: false});
  }

  render() {
    return (
      <View style={[Styles.mainWrapper]}>
        {!this.state.loading
          ? <Navbar title="مدیریت استیکرها" action="exit"/>
          : null}
        <Loading default={true} ref={(ref) => loading = ref}/>
        <ScrollView
          style={[Styles.stickersWrapper]}
          showsVerticalScrollIndicator={false}>
          <View style={[Styles.mainLayout]}>
            <Module data={this.state.data}/>
          </View>
        </ScrollView>
      </View>
    )
  }
}