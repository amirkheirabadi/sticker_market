'use strict';

import React from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Dimensions
} from 'react-native';

const width = Dimensions
    .get("window")
    .width;
const height = Dimensions
    .get("window")
    .height;
module.exports = StyleSheet.create({
    iranSans: {
        fontFamily: 'iransansmobile'
    },
    mainWrapper: {
        backgroundColor: '#fcfcfd'
    },
    mainLayout: {
        alignSelf: 'center'
    },
    navbarWrapper: {
        height: 56,
        backgroundColor: "#fcfcfd",
        zIndex: 3,
        elevation: 7,
        shadowOffset: {
            width: 30,
            height: 30
        },
        shadowColor: 'red',
        shadowOpacity: 1
    },
    navbarButton: {
        width: 22,
        height: 22,
        left: 18,
        top: 18,
        position: 'absolute'
    },
    navbarText: {
        color: '#7f57de',
        flexDirection: 'row',
        alignSelf: 'center',
        fontSize: 16,
        marginTop: 15
    },
    moduleWrapper: {
        backgroundColor: 'rgb(238, 239, 241)',
        paddingBottom: 10
    },
    GridItemWrapper: {
        marginTop: 5,
        backgroundColor: '#fff',
        borderRadius: 4,
        alignItems: 'center',
        paddingVertical: 7,
        zIndex: 1,
        elevation: 7,
        shadowOffset: {
            width: 30,
            height: 30
        },
        shadowColor: 'red',
        shadowOpacity: 0.2
    },
    titleWrapperModule: {
        padding: 10,
        paddingTop: 20,
        paddingBottom: 10
    },
    titleModule: {
        fontSize: 15
    },

    // Banner Module
    sliderWrapper: {
        height: 250
    },
    sliderLayout: {
        width: 10,
        height: 10,
        marginHorizontal: 4,
        backgroundColor: '#fff',
        alignSelf: 'flex-start'
    },

    moduleThumb: {
        width: 90,
        height: 90
    },
    moduleStickerDetail: {
        width: 80,
        height: 80
    },
    listWrapperModule: {
        padding: 10,
        paddingTop: 0,
        marginRight: 10,
        flexDirection: 'row',
        alignSelf: 'center'
    },
    listItemModule: {
        backgroundColor: '#fff',
        padding: 10,
        marginRight: 13,
        width: 130,
        alignItems: 'center',
        borderRadius: 4
    },
    lisTItemTitle: {
        marginVertical: 0
    },
    listItemMeta: {
        fontSize: 11,
        color: '#bcbcbc'
    },
    detailLayout: {
        width: width - 20,
        alignSelf: 'center',
        marginBottom: 70,
        marginTop: 10
    },
    detailsInfoWrapper: {
        flexDirection: 'row',
        width: width
    },
    detailsTitle: {
        color: '#346f9d',
        fontSize: 20,
        marginTop: 10
    },
    detailsPrice: {
        color: '#878787',
        fontSize: 12
    },
    detailsButtonWrapper: {
        backgroundColor: '#1EBE36',
        marginTop: 20,
        padding: 7,
        alignSelf: 'flex-end',
        alignItems: 'center',
        width: 100,
        height: 38
    },
    detailsDeleteButtonWrapper: {
        backgroundColor: '#e81781',
        marginTop: 20,
        padding: 7,
        alignSelf: 'flex-end',
        alignItems: 'center',
        width: 100,
        height: 38
    },
    detailsButton: {
        color: '#fff',
        fontSize: 15
    },
    detailsDescription: {
        color: '#999',
        marginTop: 10,
        fontSize: 13
    },

    // Loading
    loadingWrapper: {
        width: width,
        height: height,
        zIndex: 1,
        backgroundColor: 'rgba(255,255,255, 1)',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    loadingLayout: {
        alignSelf: 'center',
        flexDirection: 'row',
        padding: 5,
        paddingVertical: 10,
        borderRadius: 4
    },
    loadingText: {
        fontSize: 16,
        paddingRight: 20,
        alignSelf: 'flex-end'
    },
    loadingSpinner: {
        top: 10,
        alignItems: 'flex-end'
    },

    // Stickers
    stickersWrapper: {
        marginBottom: 0
    },

    // Vertical Module
    VerticalModuleWrapper: {
        backgroundColor: '#fcfcfc'
    },

    // sticker detail module
    stickerPreviewModal: {
        backgroundColor: 'rgba(255,255,255, 0.8)',
        height: height,
        width: width,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    stickerPreview: {
        width: 180,
        height: 180
    },
    infoWrapper: {
        width: width * 0.60,
        paddingRight: 30,
        paddingTop: 30
    },
    stickerInfoButtonWrapper: {
        marginVertical: 7,
        alignItems: 'flex-end'
    },
    stickerInfoButtonText: {
        fontSize: 16
    },
    stickerInfoButton: {},
    detailsCoverWrapper: {
        width: width * 0.40,
        paddingRight: 50,
        paddingBottom: 20
    },
    detailsCover: {
        width: width * 0.35,
        height: 200
    },
    stickerItemsWrapper: {
        backgroundColor: '#fff'
    },
    stickerItemsLayout: {
        paddingTop: 20,
        marginBottom: 0
    }
});